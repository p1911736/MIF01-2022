package fr.univ_lyon1.info.m1.mes;

import static org.hamcrest.Matchers.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.univ_lyon1.info.m1.mes.controller.Controller;
import fr.univ_lyon1.info.m1.mes.model.HealthProfessional;
import fr.univ_lyon1.info.m1.mes.model.Patient;
import fr.univ_lyon1.info.m1.mes.model.Prescription;
import fr.univ_lyon1.info.m1.mes.model.dao.HealthProfessionalDao;
import fr.univ_lyon1.info.m1.mes.model.dao.PatientDao;
import fr.univ_lyon1.info.m1.mes.model.factory.HealthProfessionalFactory;
import fr.univ_lyon1.info.m1.mes.model.factory.PatientFactory;

public class ControllerTest {

    Controller controller;

    @BeforeEach
    public void setUp() {
        controller = new Controller();
    }

    @Test
    public void getHpByNameTest() {
        // Given
        HealthProfessionalFactory.createHp("Dentist", "Lea");

        // When
        HealthProfessional healthProfessional = controller.getHealthProfessional("Lea");

        // Then
        assertThat(healthProfessional.getName(), is("Lea"));
    }

    @Test
    public void getPatientByIdTest() {
        // Given
        PatientFactory.createPatient("Lucas", "1");

        // When
        Patient p = controller.getPatientById("1");

        //Then
        assertThat(p.getName(), is("Lucas"));
        assertThat(p.getSSID(), is("1"));
    }

    @Test
    public void getPatientByNameTest() {
        // Given
        PatientFactory.createPatient("Hugo", "1212");
        
        // When
        Patient p = controller.getPatientByName("Hugo");

        // Then
        assertThat(p.getName(), is("Hugo"));
        assertThat(p.getSSID(), is("1212"));
    }

    @Test
    /**
     * Test if we can add a predefined prescription for a health professional from controller
     */
    public void addPredefPrescFromCtrl() {
        // Given 
        HealthProfessional hp = HealthProfessionalFactory.createHp("Kinesitherapist", "Lucas");
        controller.addProposedPrescription(hp.getName(), "Do some sport", "Fitness");

        // When
        Map<String, String> predefPresc = hp.getAllProposedPrescription();

        // Then
        for (Map.Entry<String, String> entry : predefPresc.entrySet()) {
            if (entry.getKey() == "Do some sport") {
                assertThat(entry.getValue(), is("Fitness"));
            }
        }
    }

    @Test
    public void removePrescFromCtrl() {
        // Given
        Patient p = PatientFactory.createPatient("John", "98");
        HealthProfessional hp = HealthProfessionalFactory.createHp("Homeopath", "Thomas");
        boolean added = controller.addPrescription(hp.getName(), p.getSSID(), "Eat fruits");

        // Verify that prescription has been added
        List<Prescription> prescriptions = p.getPrescriptions();
        assertThat(added, is(true));
        assertThat(prescriptions, hasItem(
            hasProperty("content", equalTo("Eat fruits"))));

        // When
        controller.removePrescription(p.getSSID(), prescriptions.get(0));

        // Then
        assertThat(prescriptions, not(hasItem(
            hasProperty("content", equalTo("Eat fruits")))));

    }

    @Test
    public void createPatientFromCtrl() {
        // Given
        String name = "Louis";
        String id = "1234567";
        try {
            controller.createPatient(name, id);

            // When
            List<Patient> patients = PatientDao.getPatientDao().findAll();

            // Then
            assertThat(patients, hasItem(
                hasProperty("name", equalTo("Louis"))));
            assertThat(patients, hasItem(
                hasProperty("SSID", equalTo("1234567"))));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createHpFromCtrl() {
        // Given
        String function = "Psychologue";
        String name = "Richard";
        controller.createHp(function, name);

        // When
        List<HealthProfessional> hps = HealthProfessionalDao.getHealthProfessionalDao().findAll();

        // Then
        assertThat(hps, hasItem(
            hasProperty("name", equalTo("Richard"))));
    }

    @Test
    public void getPatientsFromCtrl() {
        try {
            // Given
            controller.createPatient("Nico", "96325");
            controller.createPatient("Michel", "15963");

            // When
            List<Patient> patients = controller.getAllPatients();

            // Then
            assertThat(patients, hasItem(
                hasProperty("name", equalTo("Nico"))));
            assertThat(patients, hasItem(
                hasProperty("SSID", equalTo("96325"))));
            assertThat(patients, hasItem(
                hasProperty("name", equalTo("Michel"))));
            assertThat(patients, hasItem(
                hasProperty("SSID", equalTo("15963"))));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getPatientPrescriptionsFromCtrl() {
        // Given
        Patient p = PatientFactory.createPatient("Lucie", "753897");
        HealthProfessional hp = HealthProfessionalFactory.createHp("HealthProfessional", "Margot");
        controller.addPrescription(hp.getName(), p.getSSID(), "Sleep more");
        controller.addPrescription(hp.getName(), p.getSSID(), "Stop sodas");

        // When
        List<Prescription> prescriptions = controller.getAllPrescriptions(p.getSSID());

        // Then
        assertThat(prescriptions, hasItem(
            hasProperty("content", equalTo("Sleep more"))));
        assertThat(prescriptions, hasItem(
            hasProperty("content", equalTo("Stop sodas"))));
    }

    @AfterEach
    public void deletePatientFiles() {
        try {
            controller.deleteTxtFiles();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
