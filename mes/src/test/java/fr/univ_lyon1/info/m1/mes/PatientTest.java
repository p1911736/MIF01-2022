package fr.univ_lyon1.info.m1.mes;

import static org.hamcrest.Matchers.*;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.univ_lyon1.info.m1.mes.model.Patient;
import fr.univ_lyon1.info.m1.mes.model.dao.PatientDao;
import fr.univ_lyon1.info.m1.mes.model.factory.PatientFactory;

public class PatientTest {

    private PatientDao patientDao;

    private Patient patient;

    @BeforeEach
    public void setup() {
        patientDao = PatientDao.getPatientDao();
        patient = new Patient("Marc", "1238");
    }

    @Test
    public void patientCreation() {        

        // When
        PatientFactory.createPatient(patient.getName(), patient.getSSID());
        List<Patient> patients = patientDao.findAll();

        // Then
        assertThat(patients, hasItem(
            hasProperty("name", equalTo("Marc"))));
        assertThat(patients, hasItem(
            hasProperty("SSID", equalTo("1238"))));
    }

    @Test
    public void findPatientByName() {
        // When 
        PatientFactory.createPatient(patient.getName(), patient.getSSID());
        Patient p = patientDao.getPatientByName("Marc");

        // Then 
        assertThat(p.getName(), is("Marc"));
        assertThat(p.getSSID(), is("1238"));
    }

    @Test
    public void findPatientById() {
        // When 
        PatientFactory.createPatient(patient.getName(), patient.getSSID());
        Patient p = patientDao.getPatientById("1238");

        // Then 
        assertThat(p.getName(), is("Marc"));
        assertThat(p.getSSID(), is("1238"));
    }
}
