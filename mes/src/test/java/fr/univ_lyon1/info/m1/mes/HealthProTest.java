package fr.univ_lyon1.info.m1.mes;

import static org.hamcrest.Matchers.*;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.univ_lyon1.info.m1.mes.model.HealthProfessional;
import fr.univ_lyon1.info.m1.mes.model.Patient;
import fr.univ_lyon1.info.m1.mes.model.Prescription;
import fr.univ_lyon1.info.m1.mes.model.dao.PatientDao;
import fr.univ_lyon1.info.m1.mes.model.factory.HealthProfessionalFactory;
import fr.univ_lyon1.info.m1.mes.model.factory.PatientFactory;

public class HealthProTest {
    private HealthProfessional hp;

    @BeforeEach
    public void setUp() {
        hp = new HealthProfessional("Dr. Smith");
    }

    @Test
    public void HealthProfessionalName() {

        // When
        String name = hp.getName();

        // Then
        assertThat(name, is("Dr. Smith"));
    }

    @Test
    /**
     * Test addPrescription, and demonstrate advanced Hamcrest assertions.
     */
    public void GetPrescriptionTest() {
        // Given
        Patient p = PatientFactory.createPatient("Alice", "20123456789012");
        p.addPrescription(hp.getName(), "Do some sport");

        // When
        List<Prescription> prescriptions = p.getPrescriptions();

        // Then
        assertThat(prescriptions, hasItem(
            hasProperty("content", equalTo("Do some sport"))));
    }

    @Test
    /**
     * Not-so-relevant test, mostly another example of advanced assertion. More
     * relevant things to test: play with several Patients, check that a
     * prescription made for one patient doesn't apply to the other, etc.
     */
    public void GetNotPrescriptionTest() {
        // Given
        Patient p = PatientFactory.createPatient("Lucas", "1234");
        p.addPrescription(hp.getName(), "Eat fruits");

        // When
        List<Prescription> prescriptions = p.getPrescriptions();

        // Then
        assertThat(prescriptions, not(
            hasItem(
                hasProperty("content", equalTo("Do some sport")))));
    }

    @Test
    /**
     * Test removePrescription
     */
    public void removePrescriptionTest() {
        // Given
        Patient p = PatientFactory.createPatient("Alice", "321");
        p.addPrescription(hp.getName(), "Do some sport");
        List<Prescription> prescriptions = p.getPrescriptions();

        // Verify that the prescription has been added
        assertThat(prescriptions, hasItem(
            hasProperty("content", equalTo("Do some sport"))));

        // When
        p.removePrescription(prescriptions.get(0));

        // Then
        assertThat(prescriptions, not(
            hasItem(
                hasProperty("content", equalTo("Do some sport")))));
    }

    @Test
    /**
     * Check that a prescription made for one patient doesn't apply to the other
     */
    public void prescriptionDoesntApplyForOthers() {
        // Given
        Patient p = PatientFactory.createPatient("Lucas", "123");
        Patient p2 = PatientFactory.createPatient("Lea", "456");
        p.addPrescription(hp.getName(), "Eat fruits");

        // When 
        List<Prescription> prescriptionsP1 = p.getPrescriptions();
        List<Prescription> prescriptionsP2 = p2.getPrescriptions();

        // Then 
        assertThat(prescriptionsP1, hasItem(
            hasProperty("content", equalTo("Eat fruits"))));
        assertThat(prescriptionsP2, not(
            hasItem(
                hasProperty("content", equalTo("Eat fruits")))));
    }

    @Test
    /**
     * Test if we can return only return prescriptions from a specific health professional
     */
    public void getPrescriptionsFromHp() {
        // Given
        HealthProfessional hp2 = HealthProfessionalFactory.createHp("Dentist", "Lea");
        Patient p = PatientFactory.createPatient("Alice", "965");
        p.addPrescription(hp.getName(), "Do some sport");
        p.addPrescription(hp2.getName(), "Stop sugar");
        
        // When
        List<Prescription> hpPresc = p.getPrescriptions(hp.getName());
        List<Prescription> hp2Presc = p.getPrescriptions(hp2.getName());

        // Then        
        assertThat(hpPresc, hasItem(
            hasProperty("content", equalTo("Do some sport"))));
        assertThat(hpPresc, not(hasItem(
            hasProperty("content", equalTo("Stop sugar")))));

        assertThat(hp2Presc, hasItem(
            hasProperty("content", equalTo("Stop sugar"))));
        assertThat(hp2Presc, not(hasItem(
            hasProperty("content", equalTo("Do some sport")))));
    }

    @Test
    public void getAllPrescTest() {
        // Given
        Patient p = PatientFactory.createPatient("Alice", "11");
        p.addPrescription(hp.getName(), "Do some sport");
        p.addPrescription(hp.getName(), "Stop sugar");

        // When
        List<Prescription> prescriptions = PatientDao.getPatientDao().getAllPrescriptions("11");

        // Then
        assertThat(prescriptions, hasItem(
            hasProperty("content", equalTo("Do some sport"))));
        assertThat(prescriptions, hasItem(
            hasProperty("content", equalTo("Stop sugar"))));
    }
}
