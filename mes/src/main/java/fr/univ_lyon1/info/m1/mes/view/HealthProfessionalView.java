package fr.univ_lyon1.info.m1.mes.view;


import java.util.List;
import java.util.Map;
import fr.univ_lyon1.info.m1.mes.model.HealthProfessional;
import fr.univ_lyon1.info.m1.mes.model.Prescription;
import fr.univ_lyon1.info.m1.mes.model.observer.PrescriptionObserver;
import fr.univ_lyon1.info.m1.mes.controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.control.ComboBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import fr.univ_lyon1.info.m1.mes.model.strategy.Strategy;
import fr.univ_lyon1.info.m1.mes.model.strategy.NameStrategy;
import fr.univ_lyon1.info.m1.mes.model.strategy.PrefixStrategy;
import fr.univ_lyon1.info.m1.mes.model.strategy.SSIDStrategy;
import java.io.IOException;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import fr.univ_lyon1.info.m1.mes.model.Patient;

public class HealthProfessionalView implements PrescriptionObserver {
    private final VBox pane = new VBox();
    private final VBox prescriptions = new VBox();
    private final VBox historyPane = new VBox();
    private final HealthProfessional hp;
    private String selectedPatientSSID;
    private String patientSSID = null;
    private final Controller controller;
    private final Label errPatientNotFound = new Label("/!\\ Patient not found !");
    private final Label errPatientFirst = new Label("/!\\ Select a patient first !");


    public HealthProfessionalView(final HealthProfessional hp, final Controller ctrl) {
        final Stage stage = new Stage();
        stage.setTitle("Healthprofessional view");
        this.controller = ctrl;
        this.hp = hp;

        errPatientNotFound.setStyle(String.format("-fx-text-fill: RED;"));
        errPatientFirst.setStyle(String.format("-fx-text-fill: RED;"));
        errPatientNotFound.setVisible(false);
        errPatientFirst.setVisible(false);

        prescriptions.getChildren().add(new Label("PRESCRIPTIONS"));
        historyPane.getChildren().add(new Label("HISTORY PRESCRIPTIONS\n\n"));

        pane.setStyle("-fx-border-color: #CDD5D9;\n"
            + "-fx-border-insets: 5;\n"
            + "-fx-padding: 15;\n"
            + "-fx-border-width: 3;\n"
            + "-fx-background-color: #F1F1F1;\n");

        prescriptions.setStyle("-fx-border-color: #CDD5D9;\n"
            + "-fx-border-insets: 5;\n"
            + "-fx-padding: 15;\n"
            + "-fx-border-width: 2;\n"
            + "-fx-background-color: #F1F1F1;\n");

        historyPane.setStyle("-fx-border-color: #CDD5D9;\n"
        + "-fx-border-insets: 5;\n"
        + "-fx-padding: 15;\n"
        + "-fx-border-width: 2;\n"
        + "-fx-background-color: #F1F1F1;\n");

        final HBox strategy = new HBox();
        ObservableList<Strategy> strategyList = FXCollections.observableArrayList(
            new SSIDStrategy("By SSID"), 
            new NameStrategy("By Name"), 
            new PrefixStrategy("By Prefix"));
        final ComboBox cb = new ComboBox(strategyList);
        cb.getSelectionModel().select(0);
        strategy.getChildren().addAll(new Label("Strategy: "), cb);
        pane.getChildren().addAll(new Label(hp.getName()), strategy);
        final HBox search = new HBox();
        final TextField textField = new TextField();
        final Button searchButton = new Button("Search");
        search.getChildren().addAll(textField, searchButton);
        pane.getChildren().addAll(search, prescriptions, historyPane);
        pane.getChildren().add(errPatientNotFound);

        
        final EventHandler<ActionEvent> ssHandler = event -> {
            final String text = textField.getText().trim();
            if (text.equals("")) {
                errPatientNotFound.setVisible(true);
                return;
            }
            String predPatientSSID = patientSSID;
            try {
                patientSSID = controller.findPatient(((Strategy) cb.getValue()), text).getSSID();
                errPatientNotFound.setVisible(false);
                errPatientFirst.setVisible(false);
                selectedPatientSSID = patientSSID;
                controller.registerViewObserver(HealthProfessionalView.this, patientSSID);
                if (controller.getPatientById(predPatientSSID) != null) {
                    controller.unregisterViewObserver(HealthProfessionalView.this, predPatientSSID);
                }
                showPrescriptions();
            } catch (NullPointerException e) {
                errPatientNotFound.setVisible(true);
            }
            textField.setText("");
            textField.requestFocus();
        };
        searchButton.setOnAction(ssHandler);
        textField.setOnAction(ssHandler);

        pane.getChildren().add(new Label("Prescribe"));
        final HBox addPrescription = new HBox();
        final TextField tp = new TextField();
        final Button bp = new Button("Add");
        addPrescription.getChildren().addAll(tp, bp);
        pane.getChildren().addAll(addPrescription, errPatientFirst);

        final EventHandler<ActionEvent> prescriptionHandler = event -> {
            final String text = tp.getText().trim();
            if (text.equals("")) {
                errPatientFirst.setVisible(true);
                return;
            }
            tp.setText("");
            tp.requestFocus();
            this.prescribe(text);  
        };

        for (Map.Entry<String, String> mapInd 
                : controller.getIndications(hp).entrySet()) {
            final Button prescBtn = new Button(
                mapInd.getKey() + "\n" + "(" + mapInd.getValue() + ")");
            
            prescBtn.setOnAction(event -> this.prescribe(mapInd.getKey()));
            pane.getChildren().add(prescBtn);   
        }
        
        tp.setOnAction(prescriptionHandler);
        bp.setOnAction(prescriptionHandler);

        final Button logout = new Button("Logout");
        pane.getChildren().add(logout);

        logout.setOnAction(event -> {
            new AuthentificationView(new Stage(), 290, 400, controller);
            stage.close();
        });

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        scrollPane.setContent(pane);

        final Scene scene = new Scene(scrollPane, 290, 400);
        stage.setScene(scene);
        stage.show();
    }

    void prescribe(final String presc) {
        if (selectedPatientSSID == null) {
            errPatientFirst.setVisible(true);
            return;
        }
        if (!controller.addPrescription(hp.getName(), selectedPatientSSID, presc)) {
            return;
        } else {
            errPatientFirst.setVisible(false);
        }
        showPrescriptions();
    }

    void showHistoryPrescriptions() throws IOException {

        historyPane.getChildren().clear();
        
        final HBox hBox = new HBox();
        historyPane.getChildren().add(new Label("HISTORY PRESCRIPTIONS\n\n"));
        
        String str = "";
        Patient p = controller.getPatientById(patientSSID);
        for (String line : controller.listHistoryPresc(p.getName(), hp.getName())) {
            String[] splitLine = line.split("\\| ");
            str += splitLine[0] + "\n" + splitLine[1] + "\n" + "\n";
        }
        hBox.getChildren().add(new Label(str));
        historyPane.getChildren().add(hBox);
    }

    void showPrescriptions() {

        prescriptions.getChildren().clear();

        if (controller.getPatientById(patientSSID) == null) {
            prescriptions.getChildren().add(new Label("Use search above to see prescriptions"));
            return;
        }

        new Label(controller.getPatientById(patientSSID).getName());
        final HBox patientInfos = new HBox();
        patientInfos.getChildren().add(new Label("PRESCRIPTIONS"));
        prescriptions.getChildren().add(patientInfos);


        List<Prescription> listPrescriptions =
                controller.getAllPrescriptions(patientSSID);
        for (final Prescription pr : listPrescriptions) {
            final HBox prescView = new HBox();
            final Label content = new Label(
                    "- " + pr.getContent());
            prescView.getChildren().add(content);
            if (pr.getHealthProfessional().equals(hp.getName())) {
                final Button removeBtn = new Button("x");
                removeBtn.setOnAction(event -> {
                    controller.removePrescription(patientSSID, pr);
                    prescView.getChildren().remove(content);
                    prescView.getChildren().remove(removeBtn);
                });
            prescView.getChildren().add(removeBtn);
            }
            prescriptions.getChildren().add(prescView);
        }
        try {
            showHistoryPrescriptions();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void refreshHPPrescriptions() {
        if (selectedPatientSSID != null) {
            showPrescriptions();
        }
    }

    public Pane asPane() {
        return pane;
    }

    @Override
    public void update() {
        refreshHPPrescriptions();
    }
}
