package fr.univ_lyon1.info.m1.mes.model;

public class Prescription {
    private final String hp;
    private final String content;

    public String getContent() {
        return content;
    }

    public String getHealthProfessional() {
        return hp;
    }

    public Prescription(final String hp, final String content) {
        this.hp = hp;
        this.content = content;
    }
}
