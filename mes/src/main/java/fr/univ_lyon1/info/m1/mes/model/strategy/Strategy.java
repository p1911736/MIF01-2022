package fr.univ_lyon1.info.m1.mes.model.strategy;

import fr.univ_lyon1.info.m1.mes.model.Patient;

public abstract class Strategy {
    private final String name;

    public String getName() {
        return name;
    }

    public Strategy(final String name) {
        this.name = name;
    }

    public abstract Patient findPatient(String text);
}
