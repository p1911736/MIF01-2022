package fr.univ_lyon1.info.m1.mes.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fr.univ_lyon1.info.m1.mes.model.observer.PrescriptionObservable;
import fr.univ_lyon1.info.m1.mes.model.observer.PrescriptionObserver;

public class Patient implements PrescriptionObservable {
    private final List<PrescriptionObserver> listPrescriptionsObserver = new ArrayList<>();
    private final List<Prescription> prescriptions = new ArrayList<>();
    private final String name;
    private final String ssID;

    public Patient(final String name, final String ssID) {
        this.name = name;
        this.ssID = ssID;
    }

    public List<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void addPrescription(final String hp, final String content) {
        prescriptions.add(new Prescription(hp, content));
        notifyObservers();
    }

    public void removePrescription(final Prescription p) {
        prescriptions.remove(p);
        notifyObservers();
    }

    public String getName() {
        return name;
    }

    public String getSSID() {
        return ssID;
    }

    /**
     * Get prescriptions made by a specific health professional.
     * @param hp the health professional
     * @return prescriptions made by the health professional
     */
    public List<Prescription> getPrescriptions(final String hp) {
        return prescriptions.stream()
                .filter(p -> p.getHealthProfessional() == hp)
                .collect(Collectors.toList());
    }

    @Override
    public void register(final PrescriptionObserver observer) {
        listPrescriptionsObserver.add(observer);
    }

    @Override
    public void unregister(final PrescriptionObserver observer) {
        listPrescriptionsObserver.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (PrescriptionObserver observer : listPrescriptionsObserver) {
            observer.update();
        }
    }
}
