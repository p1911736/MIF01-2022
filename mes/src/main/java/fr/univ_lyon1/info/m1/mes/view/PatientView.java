package fr.univ_lyon1.info.m1.mes.view;

import fr.univ_lyon1.info.m1.mes.model.Patient;
import fr.univ_lyon1.info.m1.mes.model.Prescription;
import fr.univ_lyon1.info.m1.mes.model.observer.PrescriptionObserver;
import fr.univ_lyon1.info.m1.mes.controller.Controller;
import fr.univ_lyon1.info.m1.mes.utils.EasyClipboard;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import java.io.IOException;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;

public class PatientView implements PrescriptionObserver {
    private final Pane pane = new VBox();
    private Pane prescriptionPane = new VBox();
    private Pane historyPane = new VBox();
    private final Patient patient;
    private final Controller controller;

    public PatientView(final Patient p, final Controller ctrl) {
        final Stage stage = new Stage();
        stage.setTitle("Patient view");
        this.patient = p;
        this.controller = ctrl;
        controller.registerViewObserver(this, patient.getSSID());

        pane.setStyle("-fx-border-color: #CDD5D9;\n"
        + "-fx-border-insets: 5;\n"
        + "-fx-padding: 15;\n"
        + "-fx-border-width: 3;\n"
        + "-fx-background-color: #F1F1F1;\n");

        prescriptionPane.setStyle("-fx-border-color: #CDD5D9;\n"
        + "-fx-border-insets: 5;\n"
        + "-fx-padding: 15;\n"
        + "-fx-border-width: 2;\n"
        + "-fx-background-color: #F1F1F1;\n");

        historyPane.setStyle("-fx-border-color: #CDD5D9;\n"
        + "-fx-border-insets: 5;\n"
        + "-fx-padding: 15;\n"
        + "-fx-border-width: 2;\n"
        + "-fx-background-color: #F1F1F1;\n");
                
        final Label l = new Label("Patient " + p.getName() + " :");
        final Button bSSID = new Button("Copy id");
        bSSID.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(final ActionEvent event) {
                EasyClipboard.copy(p.getSSID());
            }
        });

        final HBox nameBox = new HBox();
        pane.getChildren().add(l);
        nameBox.getChildren().addAll(bSSID);
        pane.getChildren().addAll(nameBox, prescriptionPane, historyPane);
        showPrescriptions();
        try {
            showHistoryPrescriptions();
        } catch (IOException e) {
            e.printStackTrace();
        }

        final Button logout = new Button("Logout");
        pane.getChildren().add(logout);

        logout.setOnAction(event -> {
            new AuthentificationView(new Stage(), 290, 400, controller);
            stage.close();
        });
        
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
        scrollPane.setContent(pane);

        final Scene scene = new Scene(scrollPane, 290, 400);
        stage.setScene(scene);
        stage.show();
    }

    void showHistoryPrescriptions() throws IOException {
        
        historyPane.getChildren().clear();
        historyPane.getChildren().add(new Label("History prescriptions :\n\n"));
        
        final HBox hBox = new HBox();

        String str = "";
        for (String line : controller.listHistoryPresc(patient.getName(), null)) {
            String[] splitLine = line.split("\\| ");
            str += splitLine[0] + "\n" + splitLine[1] + "\n" + "\n";
        }
        hBox.getChildren().add(new Label(str));
        historyPane.getChildren().add(hBox);
    }

    void showPrescriptions() {
        
        prescriptionPane.getChildren().clear();
        prescriptionPane.getChildren().add(new Label("Prescriptions :\n"));
        for (final Prescription pr : patient.getPrescriptions()) {
            final HBox prescView = new HBox();
            prescView.getChildren().add(new Label("- From "
                    + pr.getHealthProfessional()
                    + ": " + pr.getContent()));
            final Button removeBtn = new Button("x");
            removeBtn.setOnAction(event -> {
                controller.removePrescription(patient.getSSID(), pr);
                prescView.getChildren().remove(pr.getContent());
                prescView.getChildren().remove(removeBtn);
            });
            prescView.getChildren().add(removeBtn);
            prescriptionPane.getChildren().add(prescView);
        }
    }

    public Pane asPane() {
        return pane;
    }

    private void refreshPatientPrescriptions() {
        showPrescriptions();
    }

    @Override
    public void update() {
        refreshPatientPrescriptions();
    }

}
