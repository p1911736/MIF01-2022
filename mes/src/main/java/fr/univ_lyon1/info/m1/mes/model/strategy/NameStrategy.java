package fr.univ_lyon1.info.m1.mes.model.strategy;

import fr.univ_lyon1.info.m1.mes.model.Patient;
import fr.univ_lyon1.info.m1.mes.model.dao.PatientDao;

public class NameStrategy extends Strategy {

    public NameStrategy(final String name) {
        super(name);
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public Patient findPatient(final String text) {
        return PatientDao.getPatientDao().findAll()
            .stream().filter(p -> p.getName().equals(text)).findFirst().orElse(null);
    }
}
