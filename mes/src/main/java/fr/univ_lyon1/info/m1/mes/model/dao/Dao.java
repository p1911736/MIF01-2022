package fr.univ_lyon1.info.m1.mes.model.dao;

import java.util.List;


public interface Dao<T> {

    void add(T element);

    void delete(T element);

    List<T> findAll();
    
}
