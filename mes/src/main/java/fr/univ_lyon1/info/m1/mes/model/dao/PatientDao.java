package fr.univ_lyon1.info.m1.mes.model.dao;

import java.util.List;
import fr.univ_lyon1.info.m1.mes.model.Patient;
import fr.univ_lyon1.info.m1.mes.model.Prescription;


public class PatientDao extends AbstractListDao<Patient> {

    private static PatientDao singleton = new PatientDao();

    public static PatientDao getPatientDao() {
        return PatientDao.singleton;
    }

    public Patient getPatientById(final String ssID) {
        for (Patient p : this.findAll()) {
            if (p.getSSID().equals(ssID)) {
                return p;
            }
        }
        return null;
    }

    public Patient getPatientByName(final String name) {
        for (Patient p : this.findAll()) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    public List<Prescription> getAllPrescriptions(final String ssID) {
        return getPatientById(ssID).getPrescriptions();
    } 
}
