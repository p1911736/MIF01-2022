package fr.univ_lyon1.info.m1.mes.model.factory;

import fr.univ_lyon1.info.m1.mes.model.HealthProfessional;
import fr.univ_lyon1.info.m1.mes.model.dao.HealthProfessionalDao;
import fr.univ_lyon1.info.m1.mes.model.Dentist;
import fr.univ_lyon1.info.m1.mes.model.Homeopath;
import fr.univ_lyon1.info.m1.mes.model.Psychologue;
import fr.univ_lyon1.info.m1.mes.model.Kinesitherapist;


public final class HealthProfessionalFactory {

    private HealthProfessionalFactory() {

    }

    public static HealthProfessional healthProfessional(final String type, final String name) {
        switch (type) {
            case "HealthProfessional": return new HealthProfessional(name);
            case "Dentist": return new Dentist(name);
            case "Homeopath": return new Homeopath(name);
            case "Psychologue": return new Psychologue(name);
            case "Kinesitherapist": return new Kinesitherapist(name);
            default: return null;
        }
    }

    public static HealthProfessional createHp(final String function, final String name) {
        HealthProfessional hp = healthProfessional(function, name);
        HealthProfessionalDao.getHealthProfessionalDao().add(hp);
        return hp;
    }
}
