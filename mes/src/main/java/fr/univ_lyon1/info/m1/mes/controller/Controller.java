package fr.univ_lyon1.info.m1.mes.controller;

import fr.univ_lyon1.info.m1.mes.model.HealthProfessional;
import fr.univ_lyon1.info.m1.mes.model.Patient;
import fr.univ_lyon1.info.m1.mes.model.Prescription;
import fr.univ_lyon1.info.m1.mes.model.observer.PrescriptionObserver;
import fr.univ_lyon1.info.m1.mes.model.dao.HealthProfessionalDao;
import fr.univ_lyon1.info.m1.mes.model.dao.PatientDao;
import fr.univ_lyon1.info.m1.mes.model.factory.HealthProfessionalFactory;
import fr.univ_lyon1.info.m1.mes.model.factory.PatientFactory;
import fr.univ_lyon1.info.m1.mes.model.strategy.Strategy;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.File;
import java.io.BufferedReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;


public class Controller {

    private List<String> functions = new ArrayList<>();
    private Map<String, String> patientsAccounts = new HashMap<String, String>();
    private Map<String, String> hpAccounts = new HashMap<String, String>();

    public HealthProfessional getHealthProfessional(final String name) {        
        return HealthProfessionalDao.getHealthProfessionalDao().getHealthProfessional(name);
    }

    public Patient getPatientById(final String id) {
        return PatientDao.getPatientDao().getPatientById(id);
    }

    public Patient getPatientByName(final String name) {
        return PatientDao.getPatientDao().getPatientByName(name);
    }

    public List<String> getFunctions() {
        return functions;
    }

    public void registerViewObserver(final PrescriptionObserver observer, 
                                     final String patientSSID) {
        getPatientById(patientSSID).register(observer);
    }

    public void unregisterViewObserver(final PrescriptionObserver observer, 
                                       final String patientSSID) {
        getPatientById(patientSSID).unregister(observer);
    }

    public List<String> listHistoryPresc(final String pName, 
                                         final String hp) throws IOException {        
        List<String> prescriptionsHistory = new ArrayList<String>();
        FileReader fileReader = new FileReader(
            String.format("./src/main/resources/txtFiles/%s.txt", pName.replaceAll("\\s", "")));
        BufferedReader reader = new BufferedReader(fileReader);

        String line = reader.readLine();

        if (hp == null) {
            while (line != null) {
                prescriptionsHistory.add(line);
                line = reader.readLine();
            }
        } else {
            while (line != null) {
                if (line.contains(hp)) {
                    prescriptionsHistory.add(line);
                }
                line = reader.readLine();
            }
        }
        reader.close();

        return prescriptionsHistory;
    }

    public String createIdPatient() {

        String id = "";

        for (int i = 0; i < 15; i++) {
            Random r = new Random();
            int n = r.nextInt(9);
            id = id + n;
        }

        return id;
    }

    public String createPassword() {

        String capitalCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "1234567890";
        String combinedChars = capitalCaseLetters + lowerCaseLetters + numbers;
        Random random = new Random();
        char[] password = new char[10];
    
        for (int i = 0; i < 10; i++) {
            password[i] = combinedChars.charAt(random.nextInt(combinedChars.length()));
        }

        return String.valueOf(password);
    }

    public boolean isPatientAccount(final String name, final String pwd) {

        for (Map.Entry<String, String> map : patientsAccounts.entrySet()) {
            if ((map.getKey().equals(name)) && (map.getValue().equals(pwd))) {
                return true;
            }
        }
        
        return false;
    }

    public boolean isHpAccount(final String name, final String pwd) {

        for (Map.Entry<String, String> map : hpAccounts.entrySet()) {
            if ((map.getKey().equals(name)) && (map.getValue().equals(pwd))) {
                return true;
            }
        }

        return false;
    }

    public void addAccountYmlFile(final String name, 
                                  final String pwd, 
                                  final String fct) throws IOException {
        
        FileWriter fileWriter = new FileWriter("./initAccounts.yml", true);
        BufferedWriter writer = new BufferedWriter(fileWriter);

        writer.write("---");
        writer.newLine();
        writer.write("name: " + name);
        writer.newLine();
        writer.write("password: " + pwd);
        writer.newLine();
        writer.write("function: " + fct);
        writer.newLine();
        writer.close();
    }

    public void addAccountMap(final String name, 
                              final String pwd, 
                              final String fct) throws IOException {

        if (fct.equals("Patient")) {
            patientsAccounts.put(name, pwd);
        } else {
            hpAccounts.put(name, pwd);
        }
    }

    public void addPrescriptionToFile(final String pName, 
                                      final String hpName, 
                                      final String content) throws IOException {
        FileWriter fileWriter = new FileWriter(String.format(
            "./src/main/resources/txtFiles/%s.txt", pName.replaceAll("\\s", "")), 
            true);
        BufferedWriter writer = new BufferedWriter(fileWriter);
        
        writer.write("[ DATE -> ");
        DateFormat format = new SimpleDateFormat("dd/MM/yy H:mm:ss ] | ");
        writer.write(format.format(new Date()));
        writer.write(hpName + " : ");
        writer.write(content);
        writer.newLine();
        writer.flush();
        writer.close();

    }

    public void createTxtFile(final String name) throws IOException {
        try {
            new FileWriter(String.format(
                "./src/main/resources/txtFiles/%s.txt", name.replaceAll("\\s", "")), 
                true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clearFile(final String path) throws IOException {

        FileWriter fileWriter = new FileWriter(path, false);
        BufferedWriter writer = new BufferedWriter(fileWriter);
        writer.write("");
        writer.flush();
        writer.close();
    }

    public void deleteTxtFiles() throws IOException {

        for (Patient p : PatientDao.getPatientDao().findAll()) {
            File file = new File(String.format(
                "./src/main/resources/txtFiles/%s.txt", p.getName().replaceAll("\\s", 
                "")));
            file.delete();
        }
    }

    public Patient createPatient(final String name, final String id) throws IOException {
        try {
            createTxtFile(name);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return PatientFactory.createPatient(name, id);
    }

    public HealthProfessional createHp(final String function, final String name) {
        if (!functions.contains(function)) {
            functions.add(function);
        }
        return HealthProfessionalFactory.createHp(function, name);
    }

    public boolean addPrescription(final String hp, final String idPatient, final String content) {
        
        Patient p = getPatientById(idPatient);
        if (p != null) {
            p.addPrescription(hp, content);
            try {
                addPrescriptionToFile(p.getName(), hp, content);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    public void addProposedPrescription(final String hp, 
                                        final String presc, 
                                        final String propPresc) {
        HealthProfessional h = HealthProfessionalDao.getHealthProfessionalDao()
                                                    .getHealthProfessional(hp);
        h.addProposedPrescription(presc, propPresc);
    }

    public void removePrescription(final String idP, final Prescription pres) {
        Patient p = getPatientById(idP);
        if (p != null) {
            p.removePrescription(pres);
        }
    }

    public Patient findPatient(final Strategy strategy, final String text) {
        return strategy.findPatient(text);
    }

    public List<HealthProfessional> getAllHP() {
        return HealthProfessionalDao.getHealthProfessionalDao().getAllHP();
    }

    public List<Patient> getAllPatients() {
        return PatientDao.getPatientDao().findAll();
    }

    public List<Prescription> getAllPrescriptions(final String idPatient) {
        return PatientDao.getPatientDao().getAllPrescriptions(idPatient);
    }

    public Map<String, String> getIndications(final HealthProfessional hp) {
        return hp.getAllProposedPrescription();
    }
}
