package fr.univ_lyon1.info.m1.mes.model.dao;

import java.util.List;
import fr.univ_lyon1.info.m1.mes.model.HealthProfessional;


public class HealthProfessionalDao extends AbstractListDao<HealthProfessional> {
    
    private static HealthProfessionalDao singleton = new HealthProfessionalDao();

    public static HealthProfessionalDao getHealthProfessionalDao() {
        return HealthProfessionalDao.singleton;
    }

    public HealthProfessional getHealthProfessional(final String name) {
        for (HealthProfessional hp : this.findAll()) {
            if (hp.getName().equals(name)) {
                return hp;
            }
        }
        return null;
    }

    public List<HealthProfessional> getAllHP() {
        return this.findAll();
    }
    
}
