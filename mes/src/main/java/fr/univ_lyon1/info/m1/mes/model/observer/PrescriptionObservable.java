package fr.univ_lyon1.info.m1.mes.model.observer;

public interface PrescriptionObservable {
    void register(PrescriptionObserver object);

    void unregister(PrescriptionObserver object);

    void notifyObservers();
}
