package fr.univ_lyon1.info.m1.mes.model.dao;

import java.util.ArrayList;
import java.util.List;


public abstract class AbstractListDao<T> implements Dao<T> {

    private List<T> elementsList = new ArrayList<>();

    @Override
    public void add(final T element) {
        elementsList.add(element);
    }

    @Override
    public void delete(final T element) {
        elementsList.remove(element);
    }

    @Override
    public List<T> findAll() {
        return elementsList;
    }
    
}
