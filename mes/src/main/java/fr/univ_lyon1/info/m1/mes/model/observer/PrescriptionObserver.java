package fr.univ_lyon1.info.m1.mes.model.observer;

public interface PrescriptionObserver {
    void update();
}
