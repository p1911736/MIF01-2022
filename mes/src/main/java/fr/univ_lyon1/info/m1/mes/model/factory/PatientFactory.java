package fr.univ_lyon1.info.m1.mes.model.factory;

import fr.univ_lyon1.info.m1.mes.model.Patient;
import fr.univ_lyon1.info.m1.mes.model.dao.PatientDao;


public final class PatientFactory {

    private PatientFactory() {

    }

    public static Patient createPatient(final String name, final String ssID) {
        Patient p = new Patient(name, ssID);
        PatientDao.getPatientDao().add(p);
        return p;
    }
}
