package fr.univ_lyon1.info.m1.mes.model;


import java.util.Map;
import java.util.HashMap;

public class HealthProfessional {

    private final String name;
    private final Map<String, String> mapPredefinedPrescriptions = new HashMap<>();

    public HealthProfessional(final String name) {
        this.name = name;
        addProposedPrescription("Paracetamol", "Pain-Killer");
    }

    public void addProposedPrescription(final String predefinedPrescription, 
                                        final String indication) {
        this.mapPredefinedPrescriptions.put(predefinedPrescription, indication); 
    }

    public Map<String, String> getAllProposedPrescription() {
        return mapPredefinedPrescriptions;
    }

    public String getName() {
        return name;
    }
}
