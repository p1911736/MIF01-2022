package fr.univ_lyon1.info.m1.mes.model;

import fr.univ_lyon1.info.m1.mes.controller.Controller;
import java.io.FileInputStream;
import java.io.InputStream;
import org.yaml.snakeyaml.Yaml;
import java.io.IOException;
import java.util.Map;

public class LoadConfigFiles {
    private Controller controller;

    public LoadConfigFiles(final Controller ctrl) throws IOException {
        
        this.controller = ctrl;
        Yaml yaml = new Yaml();

        InputStream inputPatients = new FileInputStream("./initPatients.yml");
        for (Object o : yaml.loadAll(inputPatients)) {
            Map<String, String> map = (Map<String, String>) o;

            String name = String.valueOf(map.get("name"));
            String idPatient = String.valueOf(map.get("id"));

            if ((name != null) && (idPatient != null)) {
                String pwd = controller.createPassword();
                controller.addAccountMap(name, pwd, "Patient");
                controller.addAccountYmlFile(name, pwd, "Patient");
                controller.createPatient(name, idPatient);
            }   
        }

        InputStream inputPatientsPresc = new FileInputStream("./initPatientsPrescriptions.yml");
        for (Object o : yaml.loadAll(inputPatientsPresc)) {
            Map<String, String> map = (Map<String, String>) o;

            String hp = String.valueOf(map.get("healthProfessional"));
            String idPatient = String.valueOf(map.get("idPatient"));
            String content = String.valueOf(map.get("content"));

            if ((hp != null) && (idPatient != null) && (content != null)) {
                controller.addPrescription(hp, idPatient, content);
            }
        }


        InputStream inputHealthProfessionals = new FileInputStream("./initHealthProfessionals.yml");
        for (Object o : yaml.loadAll(inputHealthProfessionals)) {
            Map<String, String> map = (Map<String, String>) o;

            String function = String.valueOf(map.get("function"));
            String name = String.valueOf(map.get("name"));

            if ((function != null) && (name != null)) {
                String pwd = controller.createPassword();
                controller.addAccountMap(name, pwd, function);
                controller.addAccountYmlFile(name, pwd, function);
                controller.createHp(function, name);
            }
        }

        InputStream inputHpPrescriptions = new FileInputStream("./initHpPrescriptions.yml");
        for (Object o : yaml.loadAll(inputHpPrescriptions)) {
            Map<String, String> map = (Map<String, String>) o;

            String nameHp = String.valueOf(map.get("nameHealthProfessional"));
            String presc = String.valueOf(map.get("prescription"));
            String propPresc = String.valueOf(map.get("proposedPrescription"));

            if ((nameHp != null) && (presc != null) && (propPresc != null)) {
                controller.addProposedPrescription(nameHp, presc, propPresc);
            }
        }  
        
        InputStream inputAccounts = new FileInputStream("./initAccounts.yml");
        for (Object o : yaml.loadAll(inputAccounts)) {
            Map<String, String> map = (Map<String, String>) o;

            String name = String.valueOf(map.get("name"));
            String password = String.valueOf(map.get("password"));
            String function = String.valueOf(map.get("function"));

            if ((name != null) && (password != null) && (function != null)) {
                controller.addAccountMap(name, password, function);

                if (function.equals("Patient")) {
                    controller.createPatient(name, controller.createIdPatient());
                } else {
                    controller.createHp(function, name);
                }
            }
        }
    } 
}
