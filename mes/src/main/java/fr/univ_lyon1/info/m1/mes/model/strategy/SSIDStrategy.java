package fr.univ_lyon1.info.m1.mes.model.strategy;

import fr.univ_lyon1.info.m1.mes.model.Patient;
import fr.univ_lyon1.info.m1.mes.model.dao.PatientDao;

public class SSIDStrategy extends Strategy {

    public SSIDStrategy(final String name) {
        super(name);
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public Patient findPatient(final String text) {
        return PatientDao.getPatientDao().getPatientById(text);
    }
}
