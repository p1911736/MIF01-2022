package fr.univ_lyon1.info.m1.mes;

import fr.univ_lyon1.info.m1.mes.controller.Controller;
import fr.univ_lyon1.info.m1.mes.view.AuthentificationView;
import javafx.application.Application;
import javafx.stage.Stage;
import fr.univ_lyon1.info.m1.mes.model.LoadConfigFiles;
import java.io.IOException;

/**
 * Main class for the application (structure imposed by JavaFX).
 */
public class App extends Application {

    private Controller controller = new Controller();
    /**
     * With javafx, start() is called when the application is launched.
     */
    @Override
    public void start(final Stage stage) throws IOException {
        
        new LoadConfigFiles(controller);
        new AuthentificationView(stage, 280, 200, controller);
        new AuthentificationView(new Stage(), 280, 200, controller);
    }

    @Override
    public void stop() {
        try {
            controller.deleteTxtFiles();
            controller.clearFile("./initAccounts.yml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * A main method in case the user launches the application using
     * App as the main class.
     */
    public static void main(final String[] args) throws IOException {
        Application.launch(args);
    }
}
