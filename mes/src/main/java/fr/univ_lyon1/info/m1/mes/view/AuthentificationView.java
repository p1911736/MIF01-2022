package fr.univ_lyon1.info.m1.mes.view;

import javafx.stage.Stage;
import javafx.scene.Scene;
import fr.univ_lyon1.info.m1.mes.controller.Controller;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.collections.FXCollections;
import java.util.List;
import java.io.IOException;

public class AuthentificationView {
    private Controller controller;
    private Stage stage;
    private HBox hBox = new HBox(5);
    private VBox vBox = new VBox(10);
    private int w;
    private int h;

    public AuthentificationView(final Stage stage, 
                                final int w, 
                                final int h, 
                                final Controller ctrl) {
        this.controller = ctrl;
        this.stage = stage;
        this.w = w;
        this.h = h;
        stage.setTitle("Login");  

        hBox.setAlignment(Pos.CENTER);

        vBox.setStyle("-fx-border-color: #CDD5D9;\n"
                + "-fx-border-insets: 5;\n"
                + "-fx-padding: 15;\n"
                + "-fx-border-width: 3;\n"
                + "-fx-background-color: #F1F1F1;\n");
        
        final TextField nameT = new TextField();
        vBox.getChildren().addAll(new Label("Name : "), nameT);
        final PasswordField pwdT = new PasswordField();
        vBox.getChildren().addAll(new Label("Password : "), pwdT);
        final Button loginB = new Button("Login");
        loginB.setPrefSize(140, 25);
        final Button signUpB = new Button("Sign up");
        signUpB.setPrefSize(140, 25);
        hBox.getChildren().addAll(loginB, signUpB);
        vBox.getChildren().add(hBox);

        loginB.setOnAction(event -> {

            String name = nameT.getText().trim();
            String pwd = pwdT.getText().trim();
            
            if ((name != "") && (pwd != "")) {
                if (controller.isPatientAccount(name, pwd)) {
                    new PatientView(controller.getPatientByName(name), controller);
                    stage.close();
                } else if (controller.isHpAccount(name, pwd)) {
                    new HealthProfessionalView(controller.getHealthProfessional(name), controller);
                    stage.close();
                }
            }
            
        });

        stage.setScene(new Scene(vBox, w, h));
        stage.show();

        signUpB.setOnAction(event -> {
            signUp();
        });
    }

    public void signUp() {
        stage.setTitle("Sign Up"); 
        stage.setHeight(h + 155);
        
        vBox.getChildren().clear();
        hBox.getChildren().clear();

        final TextField nameT = new TextField();
        vBox.getChildren().addAll(new Label("Name : "), nameT);
        final PasswordField pwdT = new PasswordField();
        vBox.getChildren().addAll(new Label("Password : "), pwdT);
        final PasswordField pwd2T = new PasswordField();
        vBox.getChildren().addAll(new Label("Rewrite the password : "), pwd2T);
        final ComboBox cb = new ComboBox();
        List<String> l1 = controller.getFunctions();
        l1.add("Patient");
        cb.setItems(FXCollections.observableArrayList(l1));
        cb.getSelectionModel().select(0);
        vBox.getChildren().addAll(new Label("Function : "), cb);
        final Button signUpB = new Button("Sign up");
        signUpB.setPrefSize(140, 25);
        final Button backB = new Button("Back");
        backB.setPrefSize(140, 25);
        hBox.getChildren().addAll(signUpB, backB);
        vBox.getChildren().add(hBox);
        
        signUpB.setOnAction(event -> {

            String name = nameT.getText().trim();
            String pwd1 = pwdT.getText().trim();
            String pwd2 = pwd2T.getText().trim();
            String fct = cb.getValue().toString();

            if ((name != "") && (pwd1 != "") && (pwd2 != "") && (fct != "")) {
                if (pwd1.equals(pwd2)) {
                    try {
                        
                        controller.addAccountMap(name, pwd1, fct);
                        controller.addAccountYmlFile(name, pwd1, fct);
                        if (fct.equals("Patient")) {
                            controller.createPatient(name, controller.createIdPatient());
                        } else {
                            controller.createHp(fct, name);
                        }
                        new AuthentificationView(stage, w, h, controller);
                        stage.setHeight(h + 30);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        backB.setOnAction(event -> {
            new AuthentificationView(stage, w, h, controller);
        });
    }
}
